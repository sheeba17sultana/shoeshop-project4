package com.commercial.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;


import com.commercial.model.Registration;
import com.commercial.model.SalesOrder;
import com.commercial.model.StockUnit;
import com.commercial.service.OrderService;
import com.commercial.service.RegistrationService;
import com.commercial.service.StockUnitService;

@RestController
@CrossOrigin

public class AppController  {

	@Autowired
	RegistrationService service;

	@Autowired
	StockUnitService stockservice;
	
	@Autowired
	OrderService orderservice;

	@PostMapping("/registrationistrationistration")
	public Registration submitRegistration(@RequestBody Registration registrationistrationistration) {

		return service.submitRegistration(registrationistrationistration);
	}

	@GetMapping("/registrationistration")
	public List<Registration> getAllRegisteredUsers() {
		// TODO Auto-generated method stub
		return service.getAllRegisteredUsers();
	}

	@GetMapping("/registrationistration/{phone}")
	public Registration getRegisteredUserByPhone(@PathVariable long phone) {
		// TODO Auto-generated method stub
		return service.getRegisteredUserByPhone(phone);
	}

	@PostMapping("/stock")
	public StockUnit createStockUnit(@RequestBody StockUnit stockUnit) {

		return stockservice.createStockUnit(stockUnit);
	}

	@PatchMapping("/stock")
	public StockUnit updateStockUnit(@RequestBody StockUnit stockUnit) {

		return stockservice.updateStockUnit(stockUnit);
	}

	@GetMapping("/stock")
	public List<StockUnit> getAllStock() {

		return stockservice.getAllStock();
	}

	@DeleteMapping("/stock/{stockId}")
	public void deleteStockUnit(@PathVariable("stockId") Integer stockId) {
		// TODO Auto-generated method stub
		stockservice.deleteStockUnit(stockId);

	}

	@GetMapping("/stock/{category}")
	public List<StockUnit> getAllStockByCategory(@PathVariable("category") String category) {
		// TODO Auto-generated method stub
		return stockservice.getAllStockByCategory(category);
	}

	@GetMapping("/clear/{clearance}")
	public List<StockUnit> getAllStockByClearance(@PathVariable("clearance") int clearance) {
		// TODO Auto-generated method stub
		return stockservice.getAllStockByClearance(clearance);
	}

	@GetMapping("/")
	public String login() {
		return ("User authentication successful");
	}

	@GetMapping("/stock/spo/{sport}")
	public List<StockUnit> getAllStockBySport(@PathVariable("sport") String sport) {
		// TODO Auto-generated method stub
		return stockservice.getAllStockBySport(sport);
	}

	@PostMapping("/order")
	public SalesOrder createOrder(@RequestBody SalesOrder salesorder) {
		// TODO Auto-generated method stub
		return orderservice.createOrder(salesorder);
	}

	@GetMapping("/order")
	public List<SalesOrder> getAllOrders() {
		// TODO Auto-generated method stub
		return orderservice.getAllOrders();
	}

}
