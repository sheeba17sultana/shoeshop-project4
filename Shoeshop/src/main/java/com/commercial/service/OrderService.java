package com.commercial.service;

import java.util.List;


import com.commercial.model.SalesOrder;


public interface OrderService {
	
	public SalesOrder createOrder(SalesOrder salesorder);
	public List<SalesOrder> getAllOrders();

}
