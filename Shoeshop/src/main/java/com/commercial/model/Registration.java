package com.commercial.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor

public class Registration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer registrationId;
	private String  fName;
	private String 	lName;
	private int age;
	private String sex;
	private String email;
	private long phone;
	private String uname;
	private String pwd;
	private String role="USER";
	
	
	
	
	

}
