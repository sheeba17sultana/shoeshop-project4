package com.commercial.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.commercial.model.SalesOrder;


public interface OrderRepository extends JpaRepository<SalesOrder, Integer> {

}
