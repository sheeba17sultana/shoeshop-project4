package com.commercial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.commercial.model.StockUnit;

@Repository
public interface StockUnitRepository extends JpaRepository<StockUnit, Integer>{


public List<StockUnit> findByCategory(String category);
public List<StockUnit> findByClearance(int clearance);
public List<StockUnit> findBySport(String sport);


	

}
