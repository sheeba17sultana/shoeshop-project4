package com.commercial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.commercial.model.Registration;

@Repository
public interface RegistrationRepository extends JpaRepository<Registration, Integer> {
	
	public Registration findByPhone(long phone);

}
