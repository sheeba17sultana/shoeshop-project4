import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';




import { AccountComponent } from './components/account/account.component';
import { WomenComponent } from './components/women/women.component';
import { MenComponent } from './components/men/men.component';
import { KiddoComponent } from './components/kiddo/kiddo.component';
import { SearchbysportComponent } from './components/searchbysport/searchbysport.component';
import { StockonclearanceComponent } from './components/stockonclearance/stockonclearance.component';
import { SignupComponent } from './components/signup/signup.component';
import { RegistrationService } from './service/registration.service';
import { StockunitService } from './service/stockunit.service';
import { AddstockComponent } from './components/addstock/addstock.component';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { LoginService } from './service/login.service';
import { LogoutComponent } from './components/logout/logout.component';
import { UpdatestockComponent } from './components/updatestock/updatestock.component';
import { DeletestockComponent } from './components/deletestock/deletestock.component';
import { ReguserlistComponent } from './components/reguserlist/reguserlist.component';
import { StocklistComponent } from './components/stocklist/stocklist.component';
import { SearchuserComponent } from './components/searchuser/searchuser.component';
import { SalesorderComponent } from './components/salesorder/salesorder.component';
import { SalesorderService } from './service/salesorder.service';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    WomenComponent,
    MenComponent,
    KiddoComponent,
    SearchbysportComponent,
    StockonclearanceComponent,
    SignupComponent,
    AddstockComponent,
    LoginComponent,
    HomeComponent,
    LogoutComponent,
    UpdatestockComponent,
    DeletestockComponent,
    ReguserlistComponent,
    StocklistComponent,
    SearchuserComponent,
    SalesorderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
    
  ],
  providers: [RegistrationService,StockunitService,LoginService,SalesorderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
