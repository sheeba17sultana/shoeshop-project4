import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Stockunit } from '../model/stockunit';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StockunitService {

  url: string;
  category: string;
  constructor(private http: HttpClient) {
    this.url = "http://localhost:9004/stock";

  }

  public getAllStockUnits(): Observable<Stockunit[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Stockunit[]>("http://localhost:9004/stock/", { headers });
  }

  public getAllStockByCategory(category: string): Observable<Stockunit[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Stockunit[]>("http://localhost:9004/stock/" + category, { headers });
  }
  public getAllStockByClearance(clearance: number): Observable<Stockunit[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Stockunit[]>("http://localhost:9004/clear/" + clearance, { headers });
  }
  public getAllStockBySport(sport: String): Observable<Stockunit[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Stockunit[]>("http://localhost:9004/stock/spo/" + sport, { headers });
  }
}
