import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OffersService {
  url:string;
  constructor(private http:HttpClient) { 
    this.url="http://localhost:9004/offers"
  }

  public getAllOffers():Observable<[]>{
    return this.http.get<[]>(this.url);
  }
}
