export class SalesOrder {
    orderId:number;
    promoCode:string;
    netPrice:number;
    discountedPrice:number;
    orderStatus:String;
}
