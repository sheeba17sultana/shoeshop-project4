import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AccountComponent } from './components/account/account.component';
import { KiddoComponent } from './components/kiddo/kiddo.component';
import { MenComponent } from './components/men/men.component';
import { SearchbysportComponent } from './components/searchbysport/searchbysport.component';

import { StockonclearanceComponent } from './components/stockonclearance/stockonclearance.component';
import { WomenComponent } from './components/women/women.component';
import { SignupComponent } from './components/signup/signup.component';
import { AddstockComponent } from './components/addstock/addstock.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { UpdatestockComponent } from './components/updatestock/updatestock.component';
import { DeletestockComponent } from './components/deletestock/deletestock.component';
import { ReguserlistComponent } from './components/reguserlist/reguserlist.component';

import { SearchuserComponent } from './components/searchuser/searchuser.component';
import { SalesorderComponent } from './components/salesorder/salesorder.component';






const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "account", component: AccountComponent },
  { path: "kiddo", component: KiddoComponent },
  { path: "men", component: MenComponent },
  { path: "searchbysport", component: SearchbysportComponent },
  { path: "stockonclearnace", component: StockonclearanceComponent },
  { path: "women", component: WomenComponent },
  { path: "signup", component: SignupComponent },
  { path: "addstock", component: AddstockComponent },
  { path: "home", component: HomeComponent },
  { path: "login", component: LoginComponent },
  { path: "logout", component: LoginComponent },
  { path: "updatestock", component: UpdatestockComponent },
  { path: "deletestock", component: DeletestockComponent },
  { path: "userreport", component: ReguserlistComponent },
  {path: "signup", component: SignupComponent },
  {path: "searchuser", component:SearchuserComponent },
  {path: "orderlist", component:SalesorderComponent }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
