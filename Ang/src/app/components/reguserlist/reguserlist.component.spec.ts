import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReguserlistComponent } from './reguserlist.component';

describe('ReguserlistComponent', () => {
  let component: ReguserlistComponent;
  let fixture: ComponentFixture<ReguserlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReguserlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReguserlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
