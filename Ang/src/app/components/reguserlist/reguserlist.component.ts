import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/service/registration.service';
import { Registration } from 'src/app/model/registration';

@Component({
  selector: 'app-reguserlist',
  templateUrl: './reguserlist.component.html',
  styleUrls: ['./reguserlist.component.css']
})
export class ReguserlistComponent implements OnInit {

  regusers:any;

  constructor(private service:RegistrationService) { }

  ngOnInit() {
    let resp=this.service.getAllUsers();
    resp.subscribe(data=>this.regusers=data);
    console.log(this.regusers);
      }

  }


