import { Component, OnInit } from '@angular/core';
import { RegistrationService } from 'src/app/service/registration.service';
import { Registration } from 'src/app/model/registration';

@Component({
  selector: 'app-searchuser',
  templateUrl: './searchuser.component.html',
  styleUrls: ['./searchuser.component.css']
})
export class SearchuserComponent implements OnInit {

  private user:Registration[];
  private phone:number;
  constructor(private service:RegistrationService) { }

  getRegisteredUserByPhone(){
    return this.service.getRegisteredUserByPhone(this.phone).subscribe(data=>{
      this.user=data;
      console.log(data);
  })
}

  ngOnInit() {
  }

}
