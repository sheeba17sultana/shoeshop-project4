import { Component, OnInit } from '@angular/core';
import { Stockunit } from 'src/app/model/stockunit';
import { StockunitService } from 'src/app/service/stockunit.service';

@Component({
  selector: 'app-women',
  templateUrl: './women.component.html',
  styleUrls: ['./women.component.css']
})
export class WomenComponent implements OnInit {
  private stocks:Stockunit[];
  private category:string;
  constructor(private service:StockunitService) { 
    this.category="women";
  }

  ngOnInit() {
    return this.service.getAllStockByCategory(this.category).subscribe(data=>{
      this.stocks=data;
      console.log(data);
      
      })
  }
}
  
