import { Component, OnInit } from '@angular/core';
import { SalesorderService } from 'src/app/service/salesorder.service';
import { SalesOrder } from 'src/app/model/sales-order';

@Component({
  selector: 'app-salesorder',
  templateUrl: './salesorder.component.html',
  styleUrls: ['./salesorder.component.css']
})
export class SalesorderComponent implements OnInit {
orders:SalesOrder[];
  constructor(private service:SalesorderService) { }

  ngOnInit() {let resp=this.service.getAllSalesOrders();
    resp.subscribe(data=>this.orders=data);
    console.log(this.orders);
      }
  }



