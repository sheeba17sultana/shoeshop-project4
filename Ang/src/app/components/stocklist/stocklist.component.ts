import { Component, OnInit } from '@angular/core';
import { StockunitService } from 'src/app/service/stockunit.service';
import { Stockunit } from 'src/app/model/stockunit';

@Component({
  selector: 'app-stocklist',
  templateUrl: './stocklist.component.html',
  styleUrls: ['./stocklist.component.css']
})
export class StocklistComponent implements OnInit {
  private stockunits:Stockunit[];
  constructor(private service:StockunitService) { }

  ngOnInit() {
    return this.service.getAllStockUnits().subscribe(data=>{
      this.stockunits=data;
      console.log(data);
  })
}
}


